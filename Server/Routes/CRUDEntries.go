package Routes

import (
	"Transportation/Config"
	"Transportation/Models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type EntriesDTO struct {
	ID           int    `json:"id"`
	DepCitiesID  int    `json:"depCitiesID"`
	DestCitiesID int    `json:"destCitiesID"`
	Name         string `json:"name"`
	Phone        string `json:"phone"`
}

func CreateEntries(c *gin.Context) {
	var err error
	var input EntriesDTO
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Println(c.ShouldBindJSON(&input))
	entries := Models.Entries{
		DepCitiesID:  input.DepCitiesID,
		DestCitiesID: input.DestCitiesID,
		Name:         input.Name,
		Phone:        input.Phone}
	if err = Config.DB.Create(&entries).Error; err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
	} else {
		c.JSON(http.StatusOK, gin.H{"id": entries.ID})
	}
}
