function __response(r: Response): Promise<any> {
    if (r.status >= 200 && r.status <300) {
        return r.json()
        } else {
            console.error(r);

            throw new Error(r.statusText)
        }
}

export function GET(url: string) {
    return fetch(url, {
        method: "GET"
    }).then(r => __response(r));
}

const __wBody = (url: string, method: string, data: Object) => fetch(url, {
    method,
    headers: { "Content-Type": "application/json;charset=utf-8" },
    body: JSON.stringify(data)
}).then(r => __response(r));

export const PUT = (url: string, data: Object = null): Promise<any> => __wBody(url, "PUT", data);