package Models

type Entries struct {
	ID           int    `json:"id" gorm:"autoIncrement"`
	DepCitiesID  int    `json:"depCitiesID"`
	DestCitiesID int    `json:"destCitiesID"`
	Name         string `json:"name"`
	Phone        string `json:"phone"`
}
