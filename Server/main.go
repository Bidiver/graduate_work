package main

import (
	"Transportation/Config"
	"Transportation/Models"
	"Transportation/Routes"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var err error

	conn := "host=localhost user=postgres password=postgres dbname=Transportation sslmode=disable"
	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	Config.DB.AutoMigrate(
		&Models.Cities{},
		&Models.Busses{},
		&Models.Flight{},
		&Models.User{},
		&Models.Entries{})

	Models.DataFilling()

	r := gin.Default()

	r.Static("/public", "../Client/public")
	r.LoadHTMLFiles("../Client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/cities", Routes.GetCities)

	r.GET("/flights", Routes.GetFlights)

	r.GET("/busses", Routes.GetBusses)

	r.PUT("/entries", Routes.CreateEntries)

	r.Run(":7000")
}
