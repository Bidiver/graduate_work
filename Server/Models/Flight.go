package Models

import "time"

// Рейс
type Flight struct {
	ID           int       `json:"id"`           // Номер (ID)
	DepDate      time.Time `json:"depDate"`      // Дата и время рейса
	TravelTime   int       `json:"travelTime"`   // Время в пути
	DepCitiesID  int       `json:"depCitiesID"`  // ID города выезда
	DestCitiesID int       `json:"destCitiesID"` // ID город назначения
	BussesID     int       `json:"bussesID"`     // ID автобуса
}
