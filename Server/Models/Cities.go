package Models

// Города
type Cities struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название

	// Связь "Один ко многим" с моделью Flight
	DepFlights   []Flight  `gorm:"foreignKey:DepCitiesID"`  // Рейсы вылета
	DestFlights  []Flight  `gorm:"foreignKey:DestCitiesID"` // Рейсы назначения
	DepFlights1  []Entries `gorm:"foreignKey:DepCitiesID"`
	DestFlights1 []Entries `gorm:"foreignKey:DestCitiesID"`
}
