export interface IFlight {
    id:            number
    depDate:       string
    travelTime:    number
    depCitiesID:      number
    destCitiesID:     number
    bussesID:    number
}

export interface IBusses {
    id:   number
    name: string
    cap:  number
}

export interface ICiti {
    id:   number
    name: string
}

export interface IEntries {
    id:               number
    depCitiesID:      number
    destCitiesID:     number
    name:             string
    phone:            string
}