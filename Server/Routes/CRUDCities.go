package Routes

import (
	"Transportation/Config"
	"Transportation/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CitiesDTO struct {
	ID   int    `json:"id"`                      // Номер (ID)
	Name string `json:"name" binding:"required"` // Название
}

type CitiesDTOUpdate struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
}

// GET /cities
// Получение всех городов
func GetCities(context *gin.Context) {
	var cities []Models.Cities
	Config.DB.Preload("DepFlights").Preload("DestFlights").Find(&cities)

	context.JSON(http.StatusOK, cities)
}
