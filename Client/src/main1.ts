import * as REST from "./REST"
import {IBusses, ICiti, IEntries, IFlight} from "./Models/interfaces";
import { Modal } from "bootstrap";

let flights: IFlight[] = [];
let busses: IBusses[] = [];
let cities: ICiti[] = [];
let entries: IEntries[] = [];

Promise.all([
    loadBusses(),
    loadCitis()
]).then(() => {
    loadFlights();
});

table_1_inv();
table_2_inv();

const but_1 = document.getElementById('button_1');
const but_2 = document.getElementById('button_2');
const but_3 = document.getElementById('button_3');

const tabFlights_button_new = document.getElementById("tabFlights_button_new");

const modalFlight_select_depCities = document.getElementById("modalFlight_select_depCities") as HTMLInputElement;
const modalFlight_select_destCities = document.getElementById("modalFlight_select_destCities") as HTMLInputElement;
const modalFlight_input_name = document.getElementById("modalFlight_input_name") as HTMLInputElement;
const modalFlight_input_phone = document.getElementById("modalFlight_input_phone") as HTMLInputElement;
const modalFlight_button_save = document.getElementById("modalFlight_button_save");

const modalFlights = new Modal(document.getElementById("modalFlight"));

but_1.addEventListener("click", main_text_vis);
but_1.addEventListener("click", table_1_inv);
but_1.addEventListener("click", table_2_inv);

but_2.addEventListener("click", main_text_inv);
but_2.addEventListener("click", table_1_vis);
but_2.addEventListener("click", table_2_inv);

but_3.addEventListener("click", main_text_inv);
but_3.addEventListener("click", table_1_inv);
but_3.addEventListener("click", table_2_vis);


function table_1_inv(){
    document.getElementById('table_1').style.display = 'none';
}
function table_1_vis(){
    document.getElementById('table_1').style.display = '';
}
function table_2_inv(){
    document.getElementById('table_2').style.display = 'none';
}
function table_2_vis(){
    document.getElementById('table_2').style.display = '';
}
function main_text_inv(){
    document.getElementById('main_text').style.display = 'none';
}
function main_text_vis(){
    document.getElementById('main_text').style.display = '';
}

tabFlights_button_new.addEventListener("click", () => {
    modalFlight_select_depCities.value = "1";
    modalFlight_select_destCities.value = "1";
    modalFlight_input_name.value = "";
    modalFlight_input_phone.value = "";
    showModalFlight();
});

function showModalFlight(){
    let flight: IFlight = null;

    modalFlights.show();
}

function loadFlights() {
    return REST.GET("/flights").then((data: IFlight[]) => {
        flights = data;

        update_tabFlights_table();
    })
}

function loadBusses(){
    return REST.GET("/busses").then((data: IBusses[]) => {
        busses = data;

    })
}

function loadCitis() {
    return REST.GET("/cities").then((data: ICiti[]) => {
        cities = data;

        update_tabCitis_table();
        update_modalFlight_select_depAiroport();
        update_modalFlight_select_destAiroport();
    })
}

function update_modalFlight_select_depAiroport() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < cities.length; i++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = cities[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("modalFlight_select_depCities");
    select.innerHTML = "";
    select.appendChild(df);
}

function update_modalFlight_select_destAiroport() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < cities.length; i ++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = cities[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("modalFlight_select_destCities");
    select.innerHTML = "";
    select.appendChild(df);
}

modalFlight_button_save.addEventListener("click", () => {
    let entrie: IEntries = {
        id: null,
        depCitiesID: Number(modalFlight_select_depCities.value),
        destCitiesID: Number(modalFlight_select_destCities.value),
        name: modalFlight_input_name.value,
        phone: modalFlight_input_phone.value
    }

    REST.PUT("/entries", entrie).then((data: any) => {
        console.log("Заявка создана ID = ", data.id);
    })
    modalFlights.hide();
});

function update_tabFlights_table() {
    const df = document.createDocumentFragment();

    for (const flight of flights) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        let Day = new Date(Date.parse(flight.depDate));
        td.innerHTML = new Intl.DateTimeFormat("def",{
            year: "numeric", month: "numeric", day: "numeric", hour: "numeric", minute: "numeric", hour12: false
        }).format(Day);
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ flight.travelTime }`;
        tr.appendChild(td);

        const depCiti = cities.find(item => item.id === flight.depCitiesID);
        td = document.createElement("td");
        td.innerHTML = depCiti.name;
        tr.appendChild(td);

        
        const destCiti = cities.find(item => item.id === flight.destCitiesID);
        td = document.createElement("td");
        td.innerHTML = destCiti.name;
        tr.appendChild(td);

        const busse = busses.find(item => item.id === flight.bussesID);
        td = document.createElement("td");
        td.innerHTML = busse.name;
        tr.appendChild(td);
        
        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody.flights");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}

function update_tabCitis_table() {
    const df = document.createDocumentFragment();

    for(let i = 0; i < cities.length; i++) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.setAttribute(`class`, `tabl`)
        td.innerHTML = cities[i].name;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_cities");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}