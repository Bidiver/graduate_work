package Routes

import (
	"Transportation/Config"
	"Transportation/Models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type FlightDTO struct {
	ID           int    `json:"id"`                               // Номер (ID)
	DepDate      string `json:"depDate"       binding:"required"` // Дата и время вылета
	TravelTime   int    `json:"travelTime"    binding:"required"` // Время в пути
	DepCitiesID  int    `json:"depCitesID"  binding:"required"`   // ID города выезда
	DestCitiesID int    `json:"destCitesID" binding:"required"`   // ID города назначения
	BussesID     int    `json:"bussesID"      binding:"required"` // ID автобуса
}

type FlightDTOUpdate struct {
	ID           int       `json:"id"`          // Номер (ID)
	DepDate      time.Time `json:"depDate"`     // Дата и время вылета
	TravelTime   int       `json:"travelTime"`  // Время в пути
	DepCiteisID  int       `json:"depCitesID" ` // ID города выезда
	DestCitiesID int       `json:"destCitesID"` // ID города назначения
	BussesID     int       `json:"bussesID"`    // ID автобуса
}

// GET /flights
// Получаем список всех рейсов
func GetFlights(context *gin.Context) {
	var flights []Models.Flight
	Config.DB.Find(&flights)

	context.JSON(http.StatusOK, flights)
}
