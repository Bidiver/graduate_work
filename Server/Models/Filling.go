package Models

import (
	"Transportation/Config"
	"math/rand"
	"time"
)

func randate() time.Time {
	min := time.Date(2022, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2023, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

func DataFilling() {
	Config.DB.Create(&User{ID: 1, Name: "Dru", Login: "qwe", Password: "123"})
	Config.DB.Create(&Busses{ID: 1, Name: "МАЗ-203", Cap: 34})
	Config.DB.Create(&Busses{ID: 2, Name: "ПАЗ-3205", Cap: 44})
	Config.DB.Create(&Busses{ID: 3, Name: "ПАЗ-4230", Cap: 37})
	Config.DB.Create(&Busses{ID: 4, Name: "КАВЗ-4238", Cap: 41})
	Config.DB.Create(&Busses{ID: 5, Name: "ЛИАЗ-5256", Cap: 23})
	Config.DB.Create(&Busses{ID: 6, Name: "НЕФАЗ-5299", Cap: 106})
	Config.DB.Create(&Cities{ID: 1, Name: "Иркутск"})
	Config.DB.Create(&Cities{ID: 2, Name: "Екатеринбург (Кольцово)"})
	Config.DB.Create(&Cities{ID: 3, Name: "Красноярск (Емельяново)"})
	Config.DB.Create(&Cities{ID: 4, Name: "Москва (Домодедово)"})
	Config.DB.Create(&Cities{ID: 5, Name: "Волгоград (Гумрак)"})
	Config.DB.Create(&Cities{ID: 6, Name: "Ростов-на-Дону (Платов)"})
	Config.DB.Create(&Flight{ID: 1, DepDate: randate(), TravelTime: 80, DepCitiesID: 1, DestCitiesID: 3, BussesID: 1})
	Config.DB.Create(&Flight{ID: 2, DepDate: randate(), TravelTime: 295, DepCitiesID: 3, DestCitiesID: 4, BussesID: 3})
	Config.DB.Create(&Flight{ID: 3, DepDate: randate(), TravelTime: 170, DepCitiesID: 2, DestCitiesID: 3, BussesID: 2})
	Config.DB.Create(&Flight{ID: 4, DepDate: randate(), TravelTime: 158, DepCitiesID: 4, DestCitiesID: 2, BussesID: 6})
	Config.DB.Create(&Flight{ID: 5, DepDate: randate(), TravelTime: 105, DepCitiesID: 4, DestCitiesID: 5, BussesID: 5})
	Config.DB.Create(&Flight{ID: 6, DepDate: randate(), TravelTime: 560, DepCitiesID: 1, DestCitiesID: 6, BussesID: 4})
}
