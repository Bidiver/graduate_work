package Models

// Автобусы
type Busses struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
	Cap  int    `json:"cap"`  // Число мест

	Flight []Flight // Рейсы
}
