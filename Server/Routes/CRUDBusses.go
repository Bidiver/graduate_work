package Routes

import (
	"Transportation/Config"
	"Transportation/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type BussesDTO struct {
	ID   int    `json:"id"`
	Name string `json:"name" binding:"required"`
	Сap  int    `json:"cap"  binding:"required"`
}

// GET /busses
// Получаем список всех самолетов
func GetBusses(context *gin.Context) {
	var busses []Models.Busses
	Config.DB.Preload("Flight").Find(&busses)

	context.JSON(http.StatusOK, busses)
}
